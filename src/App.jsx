import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css"
import { useState, useEffect } from "react";
import { AiOutlineTwitter} from 'react-icons/ai';
import {BiLogoTumblr}from 'react-icons/bi';
function App() {
  const [quotes, setquotes] = useState([])
  const [randomQuote, setRandomQuotes] = useState("")
  const [color,setColor] = useState('#111')
  useEffect(() => {
    async function fetchData() {
      const reponse = await fetch('https://type.fit/api/quotes')
      const data = await reponse.json()
      setquotes(data);
      let randomIndex = Math.floor(Math.random()* data.length)
      setRandomQuotes(data[randomIndex])
    }

    fetchData()
  }, [])
   const getNewQuote = ()=>{
      const colors=[
        "#ffa07a",
        "#f08080",
        "#fa8072 ",
        "#e66771",
        "#ffff00",
        "#f0e68c ",
      ]
      

    let randomIndex = Math.floor(Math.random() * quotes.length)
    let randomColorIndex = Math.floor(Math.random()* colors.length)
    setRandomQuotes(quotes[randomIndex])
    setColor(colors[randomColorIndex])
   }

  return (
    <div style={{backgroundColor:color, minHeight: "100vh"}}>
    <div className=" container p-5">
       <div className="card">
            <div className="card-header">
              Inspirationnel Quotes
            </div>
            <div className="card-body">
              {randomQuote ? <> <h5 className="card-title">-
                           {randomQuote.author|| "no author" }
                         </h5>
                         <p className="card-text">&quot;{randomQuote.text}&quot;</p>
              </>
               : 
               <>
                 <h2>Loading</h2>
               </>
              
              }
              <div className="row">
                <button onClick={getNewQuote} className='btn btn-primary ml-3'>New Quote</button>
                <a href={"https://twitter.com/i/flow/login?redirect_after_login=%2Fintent%2Ftweet%3Fhashtags%3Dquotes%26related%3Dfreecodecamp%26text%3D%2522You%2520can%2520never%2520cross%2520the%2520ocean%2520until%2520you%2520have%2520the%2520courage%2520to%2520lose%2520sight%2520of%2520the%2520shore.%2522%2520Christopher%2520Columbus" +
                 encodeURIComponent(' " ' + randomQuote.text + ' " ' + randomQuote.author) }
                  className='btn btn-warning' target='_blank'>
                  <AiOutlineTwitter />
                  </a>
                <a href={"https://www.tumblr.com/login?redirect_to=https%3A%2F%2Fwww.tumblr.com%2Fwidgets%2Fshare%2Ftool%3Fposttype%3Dquote%26tags%3Dquotes%252Cfreecodecamp%26caption%3DChristopher%2BColumbus%26content%3DYou%2Bcan%2Bnever%2Bcross%2Bthe%2Bocean%2Buntil%2Byou%2Bhave%2Bthe%2Bcourage%2Bto%2Blose%2Bsight%2Bof%2Bthe%2Bshore.%26canonicalUrl%3Dhttps%253A%252F%252Fwww.tumblr.com%252Fbuttons%26shareSource%3Dtumblr_share_button" +
                  encodeURIComponent( randomQuote.author) + "&content=" + encodeURIComponent( randomQuote.text)} target='_blank' className='btn btn-danger'>
                < BiLogoTumblr />
                </a>
              </div>
            </div>
       </div>
    </div>
    </div>
  );
}

export default App;
